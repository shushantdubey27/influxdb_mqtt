const mqtt = require("mqtt");
const moment = require("moment");

const devID = "INEM_DEMO";
const topic = `devicesIn/${devID}/data`;
const max = 100;
const min = 10;
let mqttClient;

function publishData() {
  const interval = setInterval(() => {
    /** Publish on mqtt every second */

    console.log("Publishing ", devID, " data...");
    const random = Math.random() * (max - min) + min;
    const random2 = Math.random() * (max - min) + min;
    const random3 = Math.random() * (max - min) + min;
    console.log("Random number", random);

    const currentHour = moment().get("hour"); // should be either 5(10:30 am in IST) or 12 (5:30 pm in IST) publish low VOLTS1 value

    const dataPacket = {
      device: devID,
      time: Date.now(),
      data: [
        {
          tag: "ACTIVE",
          value: random,
        },
        {
          tag: "CUR1",
          value: random2,
        },
        {
          tag: "CUR2",
          value: random2,
        },
        {
          tag: "CUR3",
          value: random2,
        },
        {
          tag: "FREQ",
          value: random2,
        },
        {
          tag: "MD",
          value: random2,
        },
        {
          tag: "MDKW",
          value: random2,
        },
        {
          tag: "PF1",
          value: random2,
        },
        {
          tag: "PF2",
          value: random2,
        },
        {
          tag: "PF3",
          value: random2,
        },
        {
          tag: "PFAVG",
          value: random2,
        },
        {
          tag: "REACTIVE",
          value: random2,
        },
        {
          tag: "VOLTS1",
          value: random2,
        },
        {
          tag: "VOLTS2",
          value: random2,
        },
        {
          tag: "VOLTS3",
          value: random2,
        },
        {
          tag: "W1",
          value: random2,
        },
        {
          tag: "W2",
          value: random2,
        },
        {
          tag: "W3",
          value: random2,
        },
        {
          tag: "D18",
          value: random2,
        },
        {
          tag: "D19",
          value: random2,
        },
      ],
    };

    mqttClient.publish(topic, JSON.stringify(dataPacket));
  }, 5000);
}

const mqttconfig = {
  host: "127.0.0.1",

  port: 1883,

  username: "admin",

  password: "server$4321",

  qos: 2,
};

mqttconfig.clientId = "DMFM_D2" + Date.now();

mqttClient = mqtt.connect(mqttconfig);

console.log("EnergyMeter Mqtt client connected:-", mqttClient.options.clientId);

publishData();
