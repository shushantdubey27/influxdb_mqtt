const mqtt = require('mqtt');
const Influx = require('influx');

const host = 'localhost';
const port = '1883';

const clientId = `mqtt_${Math.random().toString(16).slice(3)}`;

const connectUrl = `mqtt://${host}:${port}`;
let options = {
    clientId,
    clean: true,
    connectTimeout: 4000,
    username: 'shushant',
    password: 'bcda@321',
    reconnectPeriod: 1000,
};

const influxConn= new Influx.InfluxDB("http://localhost:8086/statistics");

influxConn.getDatabaseNames().then((names) => {
    if (!names.includes("statistics")) {
      return influxConn.createDatabase("statistics");
    }
  });

module.exports = {connectUrl, options, influxConn};