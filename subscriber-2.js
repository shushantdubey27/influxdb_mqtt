const mqtt = require("mqtt");
const influx = require("influx");
const { influxConn } = require("./config");

const devID = "INEM_DEMO";
const topic = `devicesIn/${devID}/data`;

const mqttconfig = {
  host: "127.0.0.1",
  port: 1883,
  username: "admin",
  password: "server$4321",
  qos: 2,
};

const client = mqtt.connect(mqttconfig);

client.on("connect", () => {
  client.subscribe(topic, () => {
    console.log(`Subscribe to topic ${topic}`);
  });
});

client.on("message", (topic, message) => {
  const resObj = JSON.parse(message.toString());
  
  resObj.data.map((val) => {
    influxConn
    .writePoints([
      {
        measurement: resObj.device,
        fields: { value: val.value},
        tags: { tag: val.tag },
        timestamp: resObj.time,
      },
    ])
    .then(() => {
      console.log("Added data to the Db");
    })
    .catch((err) => {
      console.log(err.message);
      console.log(err);
    });
  })
});
