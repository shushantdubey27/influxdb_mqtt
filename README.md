# InfluxDB_MQTT

PUB/SUB Model Saving of the data in InfluxDB.

## Getting Started

To run the application on your local machine you need to have node.js and mongodb installed, up and running. Once you have installed node.js and mongodb, you can now download this repository as .zip and extract it into a folder. After extracting, open the repository directory in a terminal / cmd and run the following command:

```
npm install
```

Application should now be up and running at port 3000. To access the application simply open your preffered browser and type the following in the url bar:

```
http://localhost:3000
```

To stop the server press Ctrl + C on the server terminal.

## Reference 

https://node-influx.github.io/class/src/index.js~InfluxDB.html <br/>
https://www.influxdata.com/blog/getting-started-with-node-influx/  <br/>
https://node-influx.github.io/manual/tutorial.html
